//index.js
//获取应用实例
var app = getApp()
var common = require('common.js')
var day = new Date()
var dd = day.getDate() < 10 ? '0' + day.getDate() : day.getDate()
var mm = day.getMonth() + 1 < 10 ? '0' + (day.getMonth() + 1) : (day.getMonth() + 1)
var yy = day.getFullYear()
var today = yy + '-' + mm + '-' + dd
Page({
  data: {
    dateValue: '',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    hiddenformput: true,
    modalHidden:true,
    // 页面配置  
    winWidth: 0,
    winHeight: 0,
    // tab切换 
    currentTab: 0,
    totalAmount: 0,
    bill: [],
    dateBill:[],
    color: '#93DCB7',
    type1: [
      { "name": "工资", "img":"wage", "value": "工资", "color":"#93DCB8","checked":true},
      { "name": "红包", "img": "redpacket", "value": "红包", "color": "#D94956" },
      { "name": "饮食", "img": "food", "value": "饮食", "color":"#FF8887" },
      { "name": "交通", "img": "transport", "value": "交通", "color":"#6EB2DE" },
      { "name": "衣服", "img": "cloth", "value": "衣服", "color":"#F39F60" },
      { "name": "购物", "img": "shopping", "value": "购物", "color":"#D9584A" },
      { "name": "住宿", "img": "rent", "value": "住宿", "color":"#C6B29C" },
      { "name": "其他", "img": "others", "value": "其他", "color":"#FEC400" },
    ],
    // 报表tab的bindtap数字
    selected0: true,
    selected1: false,
    selected2: false,
    selected3: true,
    selected4: false,
    selected5: false
  },
  //日期选择事件
  datePickerBindchange: function (e) {
    var dateValue = e.detail.value
    var bill = this.data.bill
    var dateBill = common.showSelDateBill(bill, dateValue)
    this.setData({
      dateValue: dateValue,
      dateBill: dateBill,
      totalAmount: common.arrAmount(dateBill)
    })
    
  },
  //打开删除明细确定页面
  showModal:function(e){
    // console.log(e)
    this.setData({
      modalHidden: false
    })
  },
  //修改明细点击事件
  modifyinput: function () {
    this.setData({
      hiddenformput: !this.data.hiddenformput
    })
  },
  //确定明细删除事件
  confirm:function(e){
    var that = this
    this.data.bill.splice(this.data.id,1)
    wx.setStorageSync('bill', this.data.bill)
    that.setData({
      modalHidden: true,
      totalAmount: common.arrAmount(that.data.bill),
      dateBill: common.showSelDateBill(that.data.bill, today),
      bill:that.data.bill
    })
  },
  //取消明细删除事件
  cancel:function(){
    this.setData({
      modalHidden:true
    })
  },
  //明细点击显示删除或者重设事件
  choseSection:function(e){
    this.setData({
      id:e.currentTarget.dataset.id
    })
  },
  //记一笔点击事件
  forminput: function () {
    // console.log(this.data.id)
    this.setData({
      hiddenformput: !this.data.hiddenformput,
      id:'新增'
    })
  },
  //取消按钮  
  formReset: function () {
    console.log(this.data.id)
    this.setData({
      hiddenformput: true,
      
    })
  },
  //确认上传  
  formSubmit: function (e) {
    var dateBill =[]
    //要获取bill的总数量的index,这里是3
    if(this.data.id=='新增'){
      //点记一笔，新增明细
      for (var i = 0 ;i < this.data.type1.length; i++) {
        if (this.data.type1[i].checked == true && i <2) {
          //选择的日期账单里添加一遍，总账单里再添加一遍
          dateBill =this.data.dateBill.push({ "amount": parseFloat(e.detail.value.amount), "type1": this.data.type1[i].name, "img": this.data.type1[i].img, "date": this.data.dateValue, "color": this.data.type1[i].color })
          this.data.bill.push({ "amount": parseFloat(e.detail.value.amount), "type1": this.data.type1[i].name, "img": this.data.type1[i].img, "date": this.data.dateValue, "color": this.data. type1[i].color })
        } else if (this.data.type1[i].checked == true && i >=2) {
          //选择的日期账单里添加一遍，总账单里再添加一遍
          this.data.dateBill.push({ "amount": 0 - parseFloat(e.detail.value.amount), "type1": this.data.type1[i].name, "img": this.data.type1[i].img, "date": this.data.dateValue, "color": this.data.type1[i].color  })
          this.data.bill.push({ "amount": 0 - parseFloat(e.detail.value.amount), "type1": this.data.type1[i].name, "img": this.data.type1[i].img, "date": this.data.dateValue, "color": this.data.type1[i].color })
        }
      }
      wx.setStorageSync('bill',this.data.bill)
      // wx.request({
      //   url:'add_section.php',
      //   data:{
      //     user:'亮亮',
      //     type1: common.type1Selected(this.data.type1),
      //     amount: (common.amountSelected(this.data.type1)) ? parseFloat(e.detail.value.amount) : 0 - parseFloat(e.detail.value.amount)
      //   },
      //   method:'POST',
      //   header: {
      //     'content-type': 'application/x-www-form-urlencoded charset=UTF-8'
      //   },

      //   success: function (res) {
      //     wx.showToast({
      //       title: '成功',
      //       icon: 'success',
      //       duration: 2000
      //     })
      //   }
      // })
    } else{
      //明细修改
      for (var i = 0 ;i < this.data.type1.length ;i++) {
        if (this.data.type1[i].checked == true && i < 2) {
          this.data.bill.splice(this.data.id, 1, { "amount": parseFloat(e.detail.value.amount), "type1": this.data.type1[i].name, "img": this.data.type1[i].img, "date": this.data.dateValue, "color": type1[i].color  })
        } else if (this.data.type1[i].checked == true && i >= 2) {
          this.data.bill.splice(this.data.id, 1, { "amount": parseFloat(0 - parseFloat(e.detail.value.amount)), "type1": this.data.type1[i].name, "img": this.data.type1[i].img, "date": this.data.dateValue, "color": type1[i].color })
        }
      }
      wx.setStorageSync('bill', this.data.bill)
    }
    // console.log(this.data.type1)
    
    this.setData({
      totalAmount: common.arrAmount(this.data.dateBill),
      bill: this.data.bill,
      dateBill: common.showSelDateBill(this.data.bill, today),
      hiddenformput: true,
    })
  },
  // 滑动切换tab 
  bindChange: function (e) {
    var that = this
    that.setData({ currentTab: e.detail.current })
  },
  // 点击tab切换 
  swichNav: function (e) {
    var that = this
    if (this.data.currentTab === e.target.dataset.current) {
      return false
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  //明细项目类别点击事件
  itemChange: function (e) {
    var checked = e.detail.value
    // console.log(checked)
    var changed = {}
    for (var i = 0 ;i < this.data.bill.length; i++) {
      if (checked.indexOf(this.data.bill[i].name) != -1) {
        changed['type1[' + i + '].checked'] = true
        changed['color'] = this.data.type1[i].color
      } else {
        changed['type1[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
  },
  //插入明细项目类别点击事件
  radioChange: function (e) {
    var checked = e.detail.value
    // console.log(checked)
    var changed = {}
    for (var i = 0 ;i < this.data.type1.length; i++) {
      if (checked.indexOf(this.data.type1[i].name) != -1) {
        changed['type1[' + i + '].checked'] = true
        changed['color'] = this.data.type1[i].color
      } else {
        changed['type1[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
  },

  //报表页面的bindtap事件，点击切换true,false
  selected0: function (e) {
    this.setData({
      selected1: false,
      selected0: true,
      selected2: false,
    })
  },
  selected1: function (e) {
    this.setData({
      selected0: false,
      selected1: true,
      selected2: false,
    })
  },
  selected2: function (e) {
    this.setData({
      selected0: false,
      selected1: false,
      selected2: true,
    })
  },
  selected3: function (e) {
    this.setData({
      selected3: true,
      selected4: false,
      selected5: false
    })
  },
  selected4: function (e) {
    this.setData({
      selected3: false,
      selected4: true,
      selected5: false
    })
  },
  selected5: function (e) {
    this.setData({
      selected3: false,
      selected4: false,
      selected5: true
    })
  },

  onLoad: function () {
    var that = this
    //获取系统日期
    var day = new Date()
    var dd = day.getDate() < 10 ? '0' + day.getDate() : day.getDate()
    var mm = day.getMonth() + 1 < 10 ? '0' + (day.getMonth()+1) : (day.getMonth()+1)
    var yy = day.getFullYear()
    var today = yy + '-' + mm + '-' + dd
    
    var bill = wx.getStorageSync('bill')
    
    that.setData({
      bill:bill,
      dateBill: common.showSelDateBill(bill, today),
      dateValue: today, //登录时获取系统日期
      totalAmount: common.arrAmount(that.data.dateBill),
      //leftAmount:totalAmount
    })
    
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        })
      }
    })
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
    
  //baobiao
  calPieAngle: function (dateBill) {
    // 计算数据总和
    dateBill = this.data.dateBill;
    let totalAmount = 0;
    dateBill.forEach((item) => {
      totalAmount +=item.amount;
    });
    // 计算出开始的弧度和所占比例
   console.log("totalAomunt"+totalAmount)
    let startAngle = 0;
    return dateBill.map((item) => {
      item.proportion = item.amount / totalAmount;
      item.startAngle = startAngle;
      startAngle += 2 * Math.PI * item.proportion;
      return item;
    });
   
  },

  onReady: function (e) {
     
    var that = this;
    that.setData({
   
      totalAmount: common.arrAmount(that.data.dateBill),
      //leftAmount:totalAmount
    })
    wx.setStorageSync('bill', that.data.bill)
    
},
 
  onShow: function (e) {
    // Do something when page show.

    var that = this;
    that.setData({

      totalAmount: common.arrAmount(that.data.dateBill),
      //leftAmount:totalAmount
    })
      wx.setStorageSync('bill', that.data.bill)
  },
  onHide: function () {
    // Do something when page hide.
  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {
    console.log('正在下拉')
  },
  onReachBottom: function () {
    // Do something when page reach bottom.
  },
  onShareAppMessage: function () {
    // return custom share data when user share.
  },
  onPageScroll: function () {
    // Do something when page scroll
  },
  

  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }

})
