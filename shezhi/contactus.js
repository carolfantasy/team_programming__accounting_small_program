var app = getApp()
Page({
  data: {
    userSuggestion:""
  },
  
  suggestion: function (e) {
    this.setData({
      userSuggestion: e.detail.value
    })
  },

  onReady: function () {

  },
  goShezhi: function () {
    wx.switchTab({
      url: '../shezhi/shezhi',
    })
  
  },

  goAlert:function(){
    console.log(this.data.userSuggestion);
    if (this.data.userSuggestion == null || this.data.userSuggestion == ""){
      wx.showToast({
        title: '未输入内容',
        duration: 2000
      })
    }else{
      wx.showToast({
        title: '成功',
        icon: 'success',
        duration: 2000
      })
    }
    
  }


})

