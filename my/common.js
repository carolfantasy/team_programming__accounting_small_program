//求数组和
function arrAmount(arr) {
  var amount = 0;
  for (var i = 0; i < arr.length; i++) {
    amount += arr[i].amount;
    // console.log(arr[i].amount);
    // console.log(arr[i].inc);
    // console.log(arr[i].type1);
  }
  return amount;

}
//创建二维数组
function createArr(i) {
  var a = new Array();  //先声明一维
  for (var k = 0; k < i; k++) {    //一维长度为i,i为变量，可以根据实际情况改变

    a[k] = new Array();  //声明二维，每一个一维数组里面的一个元素都是一个数组；

    for (var j = 0; j < 3; j++) {   //一维数组里面每个元素数组可以包含的数量p，p也是一个变量；

      a[k][j] = { amount: '', inc: '', type1: '', wo: '' };    //这里将变量初始化，我这边统一初始化为空，后面在用所需的值覆盖里面的值
    }
  }
  return a;
}
//判断被选中的类型
function type1Selected(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].checked == true) {
      return arr[i].name;
    }
  }
  return false;
}

//判断是否是收入
function amountSelected(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].checked == true && i < 2) {
      return true;
    } else if (arr[i].checked == true && i >= 2) {
      return false;
    }
  }
}
/*
//返回用户选择日期的账单
function showSelDateBill(arr, date) {
  var dateBill = []
  var arrCopy = []
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].date == date) {

      arrCopy = JSON.parse(JSON.stringify(arr[i])); //对象深拷贝
      arrCopy.index = i
      console.log(arrCopy)
      dateBill.push(arrCopy)
    }
  }
  return dateBill
}
*/
//返回用户选择年份/年月份的账单
function showSelDateBillmy(arr, mm, yy) {
  var dateBill = []
  var arrCopy = []
  for (var i = 0; i < arr.length; i++) {
    var str = arr[i].date
    str = str.replace(/-/g, '/'); // 将-替换成/，因为下面这个构造函数只支持/分隔的日期字符串
    var date = new Date(str); // 构造一个日期型数据，值为传入的字符串
    var date_yy = parseInt(date.getFullYear())
    var date_mm = parseInt(date.getMonth())
    if (mm == 0) {
      if (date_yy == yy) {
        arrCopy = JSON.parse(JSON.stringify(arr[i])); //对象深拷贝
        arrCopy.index = i
        console.log(arrCopy)
        dateBill.push(arrCopy)

      }
    }
    else if (date_mm == (mm - 1) && date_yy == yy) {
      arrCopy = JSON.parse(JSON.stringify(arr[i])); //对象深拷贝
      arrCopy.index = i
      //console.log(i)
      console.log(arrCopy)
      dateBill.push(arrCopy)
    }

  }
  return dateBill
}

module.exports.arrAmount = arrAmount;
module.exports.createArr = createArr;
module.exports.type1Selected = type1Selected;
module.exports.amountSelected = amountSelected;
//module.exports.showSelDateBill = showSelDateBill;
module.exports.showSelDateBillmy = showSelDateBillmy;