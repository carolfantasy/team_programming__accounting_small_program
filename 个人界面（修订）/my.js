// pages/my/my.js
var app = getApp()
Page({
 		
  /**
   * 页面的初始数据
   */
  data: {
 		  
     avatarUrl: "",//用户头像  
     nickName: "",//用户昵称  
      menuitems: [
        { text: '个人信息', url: '../userinfo/userinfo', icon: '../../images/usermenu/info.png', tips: '' },
        { text: '——————————————————————————————————————' },
        { text: '待办事项', url: '../userinfo/schedule?status=N', icon: '../../images/usermenu/order.png', tips:       '' },
        { text: '——————————————————————————————————————' },
        { text: '统计分析', url: '../userinfo/analysis?status=F', icon: '../../images/usermenu/history.png', tips: '' },
        { text: '——————————————————————————————————————' },
        { text: '学习笔记', url: '../userinfo/studynotes?status=Y', icon: '../../images/usermenu/huan.png', tips: '' },
          { text: '——————————————————————————————————————' },
          { text: '每日打卡', url: '../userinfo/punched-card', icon: '../../images/usermenu/punched-card.png', tips: '' },
        ]
 	 	   },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
          let that = this
            /**  
 	     * 获取用户信息  
 	     */
            wx.getUserInfo({
 	      success: function (res) {
                    console.log(res);
                    var avatarUrl = 'userInfo.avatarUrl';
                    var nickName = 'userInfo.nickName';
                    that.setData({
 	          [avatarUrl]: res.userInfo.avatarUrl,
              [nickName]: res.userInfo.nickName,
                      })
              }
      })
      /*
 	    _app.getUserInfo(function (userinfo) {
 	      console.log(userinfo)
 	      console.log(getApp().globalData.userSign)
 	      that.setData({
 	        userinfo: userinfo,
 	        userSign: getApp().globalData.userSign,
 	      })
 	    })*/
 	 	   },
 	 	  
/**
 * 生命周期函数--监听页面初次渲染完成
 */
onReady: function () {
  
    
 	 	   },
 	 	  
/**
 * 生命周期函数--监听页面显示
 */
onShow: function () {
  
        let that = this
          /*
 	    app.getUserInfo(function (userinfo) {
 	      console.log(userinfo)
 	      console.log(getApp().globalData.userSign)
 	      that.setData({
 	        userinfo: userinfo,
 	        userSign: getApp().globalData.userSign,
 	      })
 	    })*/
 	 	   },
 	 	  
/**
 * 生命周期函数--监听页面隐藏
 */
onHide: function () {
  
    
 	 	   },
 	 	  
/**
 * 生命周期函数--监听页面卸载
 */
onUnload: function () {
  
    
 	 	   },
 	 	  
/**
 * 页面相关事件处理函数--监听用户下拉动作
 */
onPullDownRefresh: function () {
  
    
 	 	   },
 	 	  
/**
 * 页面上拉触底事件的处理函数
 */
onReachBottom: function () {
  
    
 	 	   },
 	 	  
/**
 * 用户点击右上角分享
 */
onShareAppMessage: function () {
  
    
 	 	   }
})
