// pages/my/my.js
//index.js
//获取应用实例
var app = getApp()
var common = require('common.js')

var day = new Date()
var mm = parseInt(day.getMonth())+1
var yy = parseInt(day.getFullYear())
//console.log(mm+'fsdasadfsdaf')
Page({
  data: {
    dateValue: '',
    hiddenformput: true,
    modalHidden: true,
    // 页面配置  
    winWidth: 0,
    winHeight: 0,
    // tab切换 
    currentTab: 0,
    totalAmount: 0,
    bill: [],
    dateBill: [],
    objectArray0: [1,2,3,4,5,6,7,8,9,10,11,12,'-'],
    objectArray1: [2018,2019,2020,2021,2022],
    index0:mm-1,
    index1:yy-2018,
    color: '#93DCB7',
    type1: [
      { "name": "工资", "img": "wage", "value": "工资", "color": "#93DCB8", "checked": true },
      { "name": "红包", "img": "redpacket", "value": "红包", "color": "#D94956" },
      { "name": "饮食", "img": "food", "value": "饮食", "color": "#FF8887" },
      { "name": "交通", "img": "transport", "value": "交通", "color": "#6EB2DE" },
      { "name": "衣服", "img": "cloth", "value": "衣服", "color": "#F39F60" },
      { "name": "购物", "img": "shopping", "value": "购物", "color": "#D9584A" },
      { "name": "住宿", "img": "rent", "value": "住宿", "color": "#C6B29C" },
      { "name": "其他", "img": "others", "value": "其他", "color": "#FEC400" },
    ],
    // 报表tab的bindtap数字
    selected3: true,
    selected4: false
  },
  //日期选择事件
  datePickerBindchange: function (e) {
    var dateValue = e.detail.value
    var bill = this.data.bill
    var dateBill = common.showSelDateBill(bill, dateValue)
    this.setData({
      dateValue: dateValue,
      dateBill: dateBill,
      totalAmount: common.arrAmount(dateBill)
    })

  },
  //打开删除明细确定页面
  showModal: function (e) {
    // console.log(e)
    this.setData({
      modalHidden: false
    })
  },
  //修改明细点击事件
  modifyinput: function () {
    this.setData({
      hiddenformput: !this.data.hiddenformput
    })
  },
  
  // 滑动切换tab 
  bindChange: function (e) {
    var that = this
    that.setData({ currentTab: e.detail.current })
  },
  // 点击tab切换 
  swichNav: function (e) {
    var that = this
    if (this.data.currentTab === e.target.dataset.current) {
      return false
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  //明细项目类别点击事件
  itemChange: function (e) {
    var checked = e.detail.value
    // console.log(checked)
    var changed = {}
    for (var i = 0; i < this.data.bill.length; i++) {
      if (checked.indexOf(this.data.bill[i].name) != -1) {
        changed['type1[' + i + '].checked'] = true
        changed['color'] = this.data.type1[i].color
      } else {
        changed['type1[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
  },
  //插入明细项目类别点击事件
  radioChange: function (e) {
    var checked = e.detail.value
    // console.log(checked)
    var changed = {}
    for (var i = 0; i < this.data.type1.length; i++) {
      if (checked.indexOf(this.data.type1[i].name) != -1) {
        changed['type1[' + i + '].checked'] = true
        changed['color'] = this.data.type1[i].color
      } else {
        changed['type1[' + i + '].checked'] = false
      }
    }
    this.setData(changed)
  },

  //报表页面的bindtap事件，点击切换true,false
  
  selected3: function (e) {
    this.setData({
      selected3: true,
      selected4: false
    })
  },
  selected4: function (e) {
    this.setData({
      selected3: false,
      selected4: true
    })
  },
  
  onLoad: function () {
    console.log("onload~~~")
    var that = this
    //获取系统日期
    mm = parseInt(that.data.index0) + 1
    if (that.data.index0==12){
      mm = 0
    }
    yy = 2018 + parseInt(that.data.index1)
    var bill = wx.getStorageSync('bill')
    that.setData({
      bill: bill,
      //dateBill: common.showSelDateBillmy(bill, mm, yy),
      totalAmount: common.arrAmount(that.data.bill)
    })
    
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        })
      }
    })
  },
  //绘制扇形图
  calPieAngle0: function (dateBill) {
    // 计算数据总和  
    var bill = wx.getStorageSync('bill')
    var that=this
    that.setData({
      bill: bill,
      dateBill: common.showSelDateBillmy0(bill, mm, yy),
      totalAmount: common.arrAmount(that.data.dateBill)
    })
    let totalAmount = 0;
    var db=new Array(100);
    var i=0;
    dateBill.forEach((item) => {
     
        totalAmount += item.amount;
        db[i]=item;
        i++;
      
    });
    // 计算出开始的弧度和所占比例
    console.log("我就看看datebill0有没有问题：")
    console.log(dateBill)
    console.log("totalAomunt" + totalAmount)
    let startAngle = 0;
    return db.map((item) => {
        item.proportion = item.amount / totalAmount;
        item.startAngle = startAngle;
        startAngle += 2 * Math.PI * item.proportion;
        return item;  
    });

  },
  calPieAngle1: function (dateBill) {
    // 计算数据总和
    var bill = wx.getStorageSync('bill')
    var that = this
    that.setData({
      bill: bill,
      dateBill: common.showSelDateBillmy1(bill, mm, yy),
      totalAmount: common.arrAmount(that.data.dateBill)
    })
    let totalAmount = 0;
    var db = new Array(100);
    var i = 0;
    dateBill.forEach((item) => {
     
      
        totalAmount += -item.amount;
        db[i] = item;
        i++;
     

    });
    // 计算出开始的弧度和所占比例
    console.log("totalAomunt" + totalAmount)
    let startAngle = 0;

    return db.map((item) => {
      item.proportion = -item.amount / totalAmount;
      item.startAngle = startAngle;
      startAngle += 2 * Math.PI * item.proportion;
      return item;




    });

  },

  onReady: function () {
   
  },

  onShow: function () {
    console.log("onshow~~~")
    var bill = wx.getStorageSync('bill')
    console.log(bill)
    var that = this
    mm = parseInt(that.data.index0) + 1
    if (that.data.index0 == 12) {
      mm = 0
    }
    yy = 2018 + parseInt(that.data.index1)
   
    console.log(mm,yy)
    var dateBill0 = common.showSelDateBillmy0(bill, mm, yy);
    console.log("datebill0:" + dateBill0)
    var dateBill1 = common.showSelDateBillmy1(bill, mm, yy);
    console.log("datebill1:" + dateBill1)
    let pieSeries = this.calPieAngle0(dateBill0);
    var context = wx.createContext()
    var i = 0

    context.setLineWidth(2);
    context.setStrokeStyle('#ffffff');
    var width = 30, height = 10, //图例宽高
      posX = 160 + 30, posY = 30;//图例位置
    var textX = posX + width + 5, textY = posY + 10;//文本位置

    pieSeries.forEach((item) => {

      context.beginPath();
      // 设置填充颜色
      context.setFillStyle(item.color);
      // 移动到原点
      context.moveTo(100, 100);
      // 绘制弧度
      context.arc(100, 100, 80, item.startAngle, item.startAngle + 2 * Math.PI * item.proportion);
      context.closePath();
      context.fill();
      context.fillRect(posX, posY + 20 * i, width, height);
      context.moveTo(posX, posY + 20 * i);
      context.font = 'bold 12px Arial';
      var float = parseFloat(item.proportion).toFixed(4)
      var percent = item.type1 + ' : ' + float * 100 + '%';
      context.fillText(percent, textX, textY + 20 * i);
      context.stroke();
      i = i + 1;


    });
    wx.drawCanvas({
      //指定canvasId,canvas 组件的唯一标识符 
      canvasId: 'canvas0',
      actions: context.getActions()
    });
    
    let pies = this.calPieAngle1(dateBill1);
    var context = wx.createContext()
    var i = 0

    context.setLineWidth(2);
    context.setStrokeStyle('#ffffff');
    var width = 30, height = 10, //图例宽高
      posX = 160 + 30, posY = 30;//图例位置
    var textX = posX + width + 5, textY = posY + 10;//文本位置

    pies.forEach((item) => {

      context.beginPath();
      // 设置填充颜色
      context.setFillStyle(item.color);
      // 移动到原点
      context.moveTo(100, 100);
      // 绘制弧度
      context.arc(100, 100, 80, item.startAngle, item.startAngle + 2 * Math.PI * item.proportion);
      context.closePath();
      context.fill();
      context.fillRect(posX, posY + 20 * i, width, height);
      context.moveTo(posX, posY + 20 * i);
      context.font = 'bold 12px Arial';
      var float = parseFloat(item.proportion).toFixed(4)
      var percent = item.type1 + ' : ' + float * 100 + '%';
      context.fillText(percent, textX, textY + 20 * i);
      context.stroke();
      i = i + 1;


    });
    wx.drawCanvas({
      //指定canvasId,canvas 组件的唯一标识符 
      canvasId: 'canvas1',
      actions: context.getActions()
    })
  },
  onHide: function () {
    // Do something when page hide.
  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {
    console.log('正在下拉')
  },
  onReachBottom: function () {
    // Do something when page reach bottom.
  },
  onShareAppMessage: function () {
    // return custom share data when user share.
  },
  onPageScroll: function () {
    // Do something when page scroll
  },
  //点击滚轮选择月份或年份
  bindPickerChange0: function (e) {
    console.log(e.detail.value)
    this.setData({
      index0: e.detail.value
    })
    console.log("onshow~~~")
    var bill = wx.getStorageSync('bill')
    console.log(bill)
    var that = this
    mm = parseInt(that.data.index0) + 1
    if (that.data.index0 == 12) {
      mm = 0
    }
    yy = 2018 + parseInt(that.data.index1)

    console.log(mm, yy)
    456//
    var dateBill0 = common.showSelDateBillmy0(bill, mm, yy);

    console.log("datebill0:" + dateBill0)
    var dateBill1 = common.showSelDateBillmy1(bill, mm, yy);

    console.log("datebill1:" + dateBill1)
    let pieSeries = this.calPieAngle0(dateBill0);
    var context = wx.createContext()
    var i = 0

    context.setLineWidth(2);
    context.setStrokeStyle('#ffffff');
    var width = 30, height = 10, //图例宽高
      posX = 160 + 30, posY = 30;//图例位置
    var textX = posX + width + 5, textY = posY + 10;//文本位置

    pieSeries.forEach((item) => {

      context.beginPath();
      // 设置填充颜色
      context.setFillStyle(item.color);
      // 移动到原点
      context.moveTo(100, 100);
      // 绘制弧度
      context.arc(100, 100, 80, item.startAngle, item.startAngle + 2 * Math.PI * item.proportion);
      context.closePath();
      context.fill();
      context.fillRect(posX, posY + 20 * i, width, height);
      context.moveTo(posX, posY + 20 * i);
      context.font = 'bold 12px Arial';
      var float = parseFloat(item.proportion ).toFixed(4)
      var percent = item.type1 + ' : ' + float * 100 + '%';
      context.fillText(percent, textX, textY + 20 * i);
      context.stroke();
      i = i + 1;


    });
    wx.drawCanvas({
      //指定canvasId,canvas 组件的唯一标识符 
      canvasId: 'canvas0',
      actions: context.getActions()
    });

    let pies = this.calPieAngle1(dateBill1);
    var context = wx.createContext()
    var i = 0

    context.setLineWidth(2);
    context.setStrokeStyle('#ffffff');
    var width = 30, height = 10, //图例宽高
      posX = 160 + 30, posY = 30;//图例位置
    var textX = posX + width + 5, textY = posY + 10;//文本位置

    pies.forEach((item) => {

      context.beginPath();
      // 设置填充颜色
      context.setFillStyle(item.color);
      // 移动到原点
      context.moveTo(100, 100);
      // 绘制弧度
      context.arc(100, 100, 80, item.startAngle, item.startAngle + 2 * Math.PI * item.proportion);
      context.closePath();
      context.fill();
      context.fillRect(posX, posY + 20 * i, width, height);
      context.moveTo(posX, posY + 20 * i);
      context.font = 'bold 12px Arial';
      var float = parseFloat(item.proportion).toFixed(4)
      var percent = item.type1 + ' : ' + float * 100 + '%';
      context.fillText(percent, textX, textY + 20 * i);
      context.stroke();
      i = i + 1;


    });
    wx.drawCanvas({
      //指定canvasId,canvas 组件的唯一标识符 
      canvasId: 'canvas1',
      actions: context.getActions()
    })
  },
  bindPickerChange1: function (e) {
    console.log(e.detail.value)
    this.setData({
      index1: e.detail.value
    })
    console.log("onshow~~~")
    var bill = wx.getStorageSync('bill')
    console.log(bill)
    var that = this
    mm = parseInt(that.data.index0) + 1
    if (that.data.index0 == 12) {
      mm = 0
    }
    yy = 2018 + parseInt(that.data.index1)

    console.log(mm, yy)
    var dateBill0 = common.showSelDateBillmy0(bill, mm, yy);
    console.log("datebill0:" + dateBill0)
    var dateBill1 = common.showSelDateBillmy1(bill, mm, yy);
    console.log("datebill1:" + dateBill1)
    let pieSeries = this.calPieAngle0(dateBill0);
    var context = wx.createContext()
    var i = 0

    context.setLineWidth(2);
    context.setStrokeStyle('#ffffff');
    var width = 30, height = 10, //图例宽高
      posX = 160 + 30, posY = 30;//图例位置
    var textX = posX + width + 5, textY = posY + 10;//文本位置

    pieSeries.forEach((item) => {

      context.beginPath();
      // 设置填充颜色
      context.setFillStyle(item.color);
      // 移动到原点
      context.moveTo(100, 100);
      // 绘制弧度
      context.arc(100, 100, 80, item.startAngle, item.startAngle + 2 * Math.PI * item.proportion);
      context.closePath();
      context.fill();
      context.fillRect(posX, posY + 20 * i, width, height);
      context.moveTo(posX, posY + 20 * i);
      context.font = 'bold 12px Arial';
      var float = parseFloat(item.proportion).toFixed(4)
      var percent = item.type1 + ' : ' + float * 100 + '%';
      context.fillText(percent, textX, textY + 20 * i);
      context.stroke();
      i = i + 1;


    });
    wx.drawCanvas({
      //指定canvasId,canvas 组件的唯一标识符 
      canvasId: 'canvas0',
      actions: context.getActions()
    });

    let pies = this.calPieAngle1(dateBill1);
    var context = wx.createContext()
    var i = 0

    context.setLineWidth(2);
    context.setStrokeStyle('#ffffff');
    var width = 30, height = 10, //图例宽高
      posX = 160 + 30, posY = 30;//图例位置
    var textX = posX + width + 5, textY = posY + 10;//文本位置

    pies.forEach((item) => {

      context.beginPath();
      // 设置填充颜色
      context.setFillStyle(item.color);
      // 移动到原点
      context.moveTo(100, 100);
      // 绘制弧度
      context.arc(100, 100, 80, item.startAngle, item.startAngle + 2 * Math.PI * item.proportion);
      context.closePath();
      context.fill();
      context.fillRect(posX, posY + 20 * i, width, height);
      context.moveTo(posX, posY + 20 * i);
      context.font = 'bold 12px Arial';
      var float = parseFloat(item.proportion).toFixed(4)
      var percent = item.type1 + ' : ' + float * 100 + '%';
      context.fillText(percent, textX, textY + 20 * i);
      context.stroke();
      i = i + 1;


    });
    wx.drawCanvas({
      //指定canvasId,canvas 组件的唯一标识符 
      canvasId: 'canvas1',
      actions: context.getActions()
    })
  }

})
