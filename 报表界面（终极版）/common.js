//求数组和
function arrAmount(arr) {
  var amount = 0;
  for (var i = 0; i < arr.length; i++) {
    amount += arr[i].amount;
    // console.log(arr[i].amount);
    // console.log(arr[i].inc);
    // console.log(arr[i].type1);
  }
  return amount;

}
//创建二维数组
function createArr(i) {
  var a = new Array();  //先声明一维
  for (var k = 0; k < i; k++) {    //一维长度为i,i为变量，可以根据实际情况改变

    a[k] = new Array();  //声明二维，每一个一维数组里面的一个元素都是一个数组；

    for (var j = 0; j < 3; j++) {   //一维数组里面每个元素数组可以包含的数量p，p也是一个变量；

      a[k][j] = { amount: '', inc: '', type1: '', wo: '' };    //这里将变量初始化，我这边统一初始化为空，后面在用所需的值覆盖里面的值
    }
  }
  return a;
}
//判断被选中的类型
function type1Selected(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].checked == true) {
      return arr[i].name;
    }
  }
  return false;
}

//判断是否是收入
function amountSelected(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].checked == true && i < 2) {
      return true;
    } else if (arr[i].checked == true && i >= 2) {
      return false;
    }
  }
}
//返回用户选择年份/年月份的账单
function showSelDateBillmy0(arr, mm,yy) {
  var dateBill = []
  var arrCopy = []
  var arr_add=[]
  var inn = 0
  //console.log(arr.length)
  //console.log(arr)
  var len=0
  for (var i = 0; i < arr.length; i++){
    if(arr[i].amount>0){
      arr_add[len] = JSON.parse(JSON.stringify(arr[i])); //对象深拷贝
      len++
    }
      
  }
  console.log("收入的明细")
  console.log(arr_add)
  for (var i = 0; i < arr_add.length; i++) {
    var str = arr_add[i].date
    str = str.replace(/-/g, '/'); // 将-替换成/，因为下面这个构造函数只支持/分隔的日期字符串
    var date = new Date(str); // 构造一个日期型数据，值为传入的字符串
    var date_yy=parseInt(date.getFullYear())
    var date_mm = parseInt(date.getMonth())
    
    if (mm == 0){
      if (date_yy == yy){
        if(inn==0){
          arrCopy[inn] = JSON.parse(JSON.stringify(arr_add[i])); //对象深拷贝
          inn = inn + 1
        }else{
          var flag = true
          for (var j = 0; j < inn; j++) {
            if (arr_add[i].type1 == arrCopy[j].type1) {
              //console.log("原数组索引"+i+"更改后索引"+inn+arr[i].type1)
              arrCopy[j].amount = arrCopy[j].amount + arr_add[i].amount
              //console.log(arrCopy[j].amount)
              flag=false
            }
          }
           if(flag==true) {
             arrCopy[inn] = JSON.parse(JSON.stringify(arr_add[i])); //对象深拷贝
             inn = inn + 1
          }
        }
       
      }
    }
    
    else if (date_mm == (mm - 1) && date_yy == yy) {
      if (inn == 0) {
        
        arrCopy[inn] = JSON.parse(JSON.stringify(arr_add[i])); //对象深拷贝
        inn = inn + 1
      } else {
        var flag = true
        for (var j = 0; j <inn; j++) {
          if (arr_add[i].type1 == arrCopy[j].type1) {
            //console.log("原数组索引"+i+"更改后索引"+inn+arr[i].type1)
            //console.log("第" + inn + "个明细chongfu")
            arrCopy[j].amount = arrCopy[j].amount + arr_add[i].amount
            console.log(arrCopy[j])
            flag = false
          }

        }
        if (flag == true) {
          //console.log("第"+inn+"个明细")
          arrCopy[inn] = JSON.parse(JSON.stringify(arr_add[i])); //对象深拷贝
          inn = inn + 1
        }
      }
      //console.log("收入的账单：" + arrCopy[inn-1])
    }

    
  }
  //console.log("收入的长度："+arrCopy.length)
  for (var j = 0; j < inn; j++) {
    console.log(arrCopy[j])
    dateBill.push(arrCopy[j])
  }
  console.log("收入的总帐单")
  console.log(arrCopy)
  return dateBill
}
function showSelDateBillmy1(arr, mm, yy) {
  var dateBill = []
  var arrCopy = []
  var arr_sub = []
  var inn = 0
  console.log(arr.length)
  console.log(arr)
  var len = 0
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].amount < 0){
      arr_sub[len] = JSON.parse(JSON.stringify(arr[i])); //对象深拷贝
      len++
    }
     
  }
  console.log("支出的明细")
  console.log(arr_sub)
  for (var i = 0; i < arr_sub.length; i++) {
    var str = arr_sub[i].date
    str = str.replace(/-/g, '/'); // 将-替换成/，因为下面这个构造函数只支持/分隔的日期字符串
    var date = new Date(str); // 构造一个日期型数据，值为传入的字符串
    var date_yy = parseInt(date.getFullYear())
    var date_mm = parseInt(date.getMonth())

    if (mm == 0) {
      if (date_yy == yy) {
        if (inn == 0) {
          arrCopy[inn] = JSON.parse(JSON.stringify(arr_sub[i])); //对象深拷贝
          inn = inn + 1
        } else {
          var flag = true
          for (var j = 0; j < inn; j++) {
           
            if (arr_sub[i].type1 == arrCopy[j].type1) {
              //console.log("原数组索引"+i+"更改后索引"+inn+arr[i].type1)
              arrCopy[j].amount = arrCopy[j].amount + arr_sub[i].amount
              //console.log(arrCopy[j].amount)
              flag = false
            }

          }
          if (flag == true) {
            arrCopy[inn] = JSON.parse(JSON.stringify(arr_sub[i])); //对象深拷贝
            inn = inn + 1
          }
        }

      }
    }

    else if (date_mm == (mm - 1) && date_yy == yy) {
      if (inn == 0) {
        arrCopy[inn] = JSON.parse(JSON.stringify(arr_sub[i])); //对象深拷贝
        inn = inn + 1
      } else {
        var flag = true
        for (var j = 0; j < inn; j++) {
          if (arr_sub[i].type1 == arrCopy[j].type1) {
            //console.log("原数组索引"+i+"更改后索引"+inn+arr[i].type1)
            arrCopy[j].amount = arrCopy[j].amount + arr_sub[i].amount
            //console.log(arrCopy[j].amount)
            flag = false
          }

        }
        if (flag == true) {
          arrCopy[inn] = JSON.parse(JSON.stringify(arr_sub[i])); //对象深拷贝
          inn = inn + 1
        }
      }
    }


  }
  for (var j = 0; j < inn; j++) {
    
    console.log(arrCopy[j])
    dateBill.push(arrCopy[j])
  }
  console.log("支出的总帐单")
  console.log(arrCopy)
  return dateBill
}
/*
function showSelDateBillmy(arr_add, mm, yy) {
  var dateBill = []
  var arrCopy = []
  
  var inn = 0
  
 
  for (var i = 0; i < arr_add.length; i++) {
    var str = arr_add[i].date
    str = str.replace(/-/g, '/'); // 将-替换成/，因为下面这个构造函数只支持/分隔的日期字符串
    var date = new Date(str); // 构造一个日期型数据，值为传入的字符串
    var date_yy = parseInt(date.getFullYear())
    var date_mm = parseInt(date.getMonth())

    if (mm == 0) {
      if (date_yy == yy) {
        if (inn == 0) {
          arrCopy[inn] = JSON.parse(JSON.stringify(arr_add[i])); //对象深拷贝
          inn = inn + 1
        } else {
          for (var j = 0; j < inn; j++) {
            var flag = true
            if (arr_add[i].type1 == arrCopy[j].type1) {
              //console.log("原数组索引"+i+"更改后索引"+inn+arr[i].type1)
              arrCopy[j].amount = arrCopy[j].amount + arr_add[i].amount
              //console.log(arrCopy[j].amount)
              flag = false
            }

          }
          if (flag == true) {
            arrCopy[inn] = JSON.parse(JSON.stringify(arr_add[i])); //对象深拷贝
            inn = inn + 1
          }
        }

      }
    }

    else if (date_mm == (mm - 1) && date_yy == yy) {
      if (inn == 0) {
        arrCopy[inn] = JSON.parse(JSON.stringify(arr_add[i])); //对象深拷贝
        inn = inn + 1
      } else {
        for (var j = 0; j < inn; j++) {
          var flag = true
          if (arr_add[i].type1 == arrCopy[j].type1) {
            //console.log("原数组索引"+i+"更改后索引"+inn+arr[i].type1)
            arrCopy[j].amount = arrCopy[j].amount + arr_add[i].amount
            //console.log(arrCopy[j].amount)
            flag = false
          }

        }
        if (flag == true) {
          arrCopy[inn] = JSON.parse(JSON.stringify(arr_add[i])); //对象深拷贝
          inn = inn + 1
        }
      }
    }


  }
  for (var j = 0; j < inn; j++) {
    console.log(arrCopy[j])
    dateBill.push(arrCopy[j])
  }
  return dateBill
}*/
module.exports.arrAmount = arrAmount;
module.exports.createArr = createArr;
module.exports.type1Selected = type1Selected;
module.exports.amountSelected = amountSelected;
module.exports.showSelDateBillmy0 = showSelDateBillmy0;
module.exports.showSelDateBillmy1 = showSelDateBillmy1;
//module.exports.showSelDateBillmy = showSelDateBillmy;